using UiScroll.Code.Infrastructure.StateMachine;

namespace UiScroll.Code.Infrastructure.Boot
{
    public class Game
    {
        public readonly IGameStateMachine GameStateMachine;
        public Game(IGameStateMachine gameStateMachine) => GameStateMachine = gameStateMachine;
    }
}