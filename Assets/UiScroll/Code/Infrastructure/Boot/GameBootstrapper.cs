using UiScroll.Code.Infrastructure.StateMachine;
using UiScroll.Code.Infrastructure.StateMachine.States;
using UiScroll.Code.Services.LoadData;
using UiScroll.Code.Services.LoadingCurtain;
using UnityEngine;

namespace UiScroll.Code.Infrastructure.Boot
{
    public class GameBootstrapper : MonoBehaviour, ICoroutineRunner
    {  
        public LoadingCurtain _curtain;
        public LoadDataService _LoadDataService;
        private Game _game;

        private void Awake()
        {
            _game = new Game(new GameStateMachine(ServiceContainer.ServiceContainer.Container, _curtain, _LoadDataService,  this));
            _game.GameStateMachine.Enter<BootstrapState>();
            DontDestroyOnLoad(this);
        }
    }
}