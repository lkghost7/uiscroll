using System.Collections;
using UnityEngine;

namespace UiScroll.Code.Infrastructure
{
    public interface ICoroutineRunner
    {
        Coroutine StartCoroutine(IEnumerator enumerator);
    }
}