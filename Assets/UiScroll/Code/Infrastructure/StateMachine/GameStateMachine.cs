using System;
using System.Collections.Generic;
using UiScroll.Code.Infrastructure.StateMachine.States;
using UiScroll.Code.Services.Factories.UIFactory;
using UiScroll.Code.Services.LoadData;
using UiScroll.Code.Services.LoadingCurtain;
using UiScroll.Code.Services.SceneLoader;
using UiScroll.Code.Services.StaticData;

namespace UiScroll.Code.Infrastructure.StateMachine
{
    public class GameStateMachine : IGameStateMachine
    {
        private readonly Dictionary<Type, IExitableState> _states;
        private IExitableState _activeState;

        public GameStateMachine(ServiceContainer.ServiceContainer container,LoadingCurtain loadingCurtain, LoadDataService loadDataService, ICoroutineRunner coroutineRunner)
        {
            _states = new Dictionary<Type, IExitableState>()
            {
                [typeof(BootstrapState)] = new BootstrapState(this, container),
                [typeof(LoadProgressState)] = new LoadProgressState(this,  container.Single<IStaticData>(), loadingCurtain, loadDataService),
                [typeof(MainMenuState)] = new MainMenuState(this, container.Single<ISceneLoader>(), container.Single<IUIFactory>(), container.Single<IStaticData>()),
            };
        }

        public void Enter<TState>() where TState : class, IState =>
            ChangeState<TState>().Enter();

        public void Enter<TState, TPayload>(TPayload payload) where TState : class, IPayloadedState<TPayload> =>
            ChangeState<TState>().Enter(payload);

        private TState GetState<TState>() where TState : class, IExitableState
            => _states[typeof(TState)] as TState;

        private TState ChangeState<TState>() where TState : class, IExitableState
        {
            _activeState?.Exit();
            TState state = GetState<TState>();
            _activeState = state;
            return state;
        }
        
        ~GameStateMachine() => _activeState.Exit();
    }
}