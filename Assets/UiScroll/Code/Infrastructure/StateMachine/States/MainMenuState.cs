using UiScroll.Code.Services.Factories.UIFactory;
using UiScroll.Code.Services.SceneLoader;
using UiScroll.Code.Services.StaticData;
using UnityEngine;

namespace UiScroll.Code.Infrastructure.StateMachine.States
{
    public class MainMenuState : IState
    {
        private readonly IGameStateMachine _stateMachine;
        private readonly ISceneLoader _sceneLoader;
        private readonly IUIFactory _uiFactory;
        private readonly IStaticData _staticDataService;

        private const string MainMenuSceneName = "MainMenu";

        public MainMenuState(IGameStateMachine stateMachine, ISceneLoader sceneLoader, 
            IUIFactory uiFactory,  IStaticData staticDataService)
        {
            
            _uiFactory = uiFactory;
            _staticDataService = staticDataService;
            _sceneLoader = sceneLoader;
            _stateMachine = stateMachine;
        }
        
        public void Enter() => _sceneLoader.LoadScene(MainMenuSceneName, CreateUI);

        public void Exit() { }
 
        private void CreateUI()
        {
            _uiFactory.ActiveScene = MainMenuSceneName;
            GameObject rootUi = _uiFactory.CreateRootCanvas();
            _uiFactory.CreateScrollView(rootUi.transform);
            _uiFactory.CreateTitleView(rootUi.transform);
            _uiFactory.CreateUiItems();
        }
    }
}