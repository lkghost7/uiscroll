using UiScroll.Code.Services.Dispose;
using UiScroll.Code.Services.Factories.UIFactory;
using UiScroll.Code.Services.Providers.AssetProvider;
using UiScroll.Code.Services.Providers.StaticDataProvider;
using UiScroll.Code.Services.SceneLoader;
using UiScroll.Code.Services.StaticData;

namespace UiScroll.Code.Infrastructure.StateMachine.States
{
    public class BootstrapState : IState
    {
        private readonly IGameStateMachine _gameStateMachine;

        public BootstrapState(IGameStateMachine gameStateMachine, ServiceContainer.ServiceContainer container)
        {
            _gameStateMachine = gameStateMachine;
            RegisterServices(container);
        }

        public void Enter() => _gameStateMachine.Enter<LoadProgressState>();

        public void Exit()
        {
        }

        private void RegisterServices(ServiceContainer.ServiceContainer container)
        {
            container.RegisterSingle<IGameStateMachine>(_gameStateMachine);
            container.RegisterSingle<IDisposeService>(new DisposeService());
            container.RegisterSingle<IAssetProvider>(new AssetProvider());
            container.RegisterSingle<IStaticDataProvider>(new StaticDataProvider());
            container.RegisterSingle<ISceneLoader>(new SceneLoader(container.Single<IDisposeService>()));
            RegisterStaticData(container);
            RegisterUIFactory(container);
        }

        private void RegisterUIFactory(ServiceContainer.ServiceContainer container)
        {
            container.RegisterSingle<IUIFactory>(new UIFactory(
                _gameStateMachine,
                container.Single<IAssetProvider>(),
                container.Single<IStaticData>(),
                container.Single<IDisposeService>()));
        }

        private void RegisterStaticData(ServiceContainer.ServiceContainer container)
        {
            IStaticData staticData = new StaticData(container.Single<IStaticDataProvider>());
            staticData.LoadStaticData();
            container.RegisterSingle<IStaticData>(staticData);
        }
    }
}