using UiScroll.Code.Services.LoadData;
using UiScroll.Code.Services.LoadingCurtain;
using UiScroll.Code.Services.StaticData;
using UnityEngine;
using UnityEngine.Networking;

namespace UiScroll.Code.Infrastructure.StateMachine.States
{
    public class LoadProgressState : IState
    {
        private readonly IGameStateMachine _stateMachine;
        private readonly IStaticData _staticDataService;
        private readonly LoadingCurtain _loadingCurtain;
        private readonly LoadDataService _loadDataService;

        public LoadProgressState(IGameStateMachine stateMachine,
            IStaticData staticDataService, LoadingCurtain loadingCurtain,
            LoadDataService loadDataService)
        {
            _staticDataService = staticDataService;
            _loadingCurtain = loadingCurtain;
            _stateMachine = stateMachine;
            _loadDataService = loadDataService;
        }
 
        public void Enter()
        {
            _staticDataService.GameConfig.ClearItem();
            _loadDataService.OnCountLoad += UpdateCount;
            _loadDataService.OnLoadComplete += LoadComplete;
            _loadDataService.LoadData(_staticDataService.GameConfig, CheckConnection);
        }
        
        public void Exit()
        {
            _loadingCurtain.Hide();
            _loadDataService.OnCountLoad -= UpdateCount;
            _loadDataService.OnLoadComplete -= LoadComplete;
        }

        private void CheckConnection(UnityWebRequest unityWebRequest) => Debug.Log("unityWebRequest status - " + unityWebRequest.result);

        private void LoadComplete() => _stateMachine.Enter<MainMenuState>();
        private void UpdateCount(int count) => _loadingCurtain.UpdateLoadingText(count);
    }
}