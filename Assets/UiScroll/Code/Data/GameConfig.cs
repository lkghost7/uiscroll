﻿using System.Collections.Generic;
using UnityEngine;

namespace UiScroll.Code.Data
{
    [CreateAssetMenu(fileName = "Game Config", menuName = "Static Data/Game Config")]
    public class GameConfig : ScriptableObject
    {
        [Header("Links data")]
        public string LinkData = "https://dfu8aq28s73xi.cloudfront.net/testUsers";
        public string LinkDataText = "https://gitlab.com/lkghost7/uiscroll/-/raw/main/phone.txt?inline=false";
        
        [Header("Data const")]
        public string Username = "Username";
        public string Points = "Points";
        public string AvatarUrl = "AvatarUrl";

        [Header("Item data")] 
        public List<UiItem> UiItems;

        public void ClearItem() => UiItems.Clear();
        public void AddUiItem(UiItem item) => UiItems.Add(item);

        public void AddPhoto(UiItem item, Sprite sprite)
        {
            // if (UiItems.Contains(item))
            //     item.Photo = sprite;
        }

        // public void SortRank()
        // {
        //     List<UiItem> sortRang = UiItems.OrderByDescending(r => r.Score).ToList();
        //     UiItems.Clear();
        //     UiItems.AddRange(sortRang);
        // }
    }
}