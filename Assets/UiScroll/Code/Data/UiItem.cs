﻿using System;
using UnityEngine;

namespace UiScroll.Code.Data
{
    [Serializable]
    
    public class UiItem
    {
        public int ID;
        public string Title;
        public string NickName;
        public string PhoneNumber;
        public bool isTitle;
        // public int Score;
        // public Sprite Photo;
    }
}