﻿using System;
using System.Text.RegularExpressions;
using TMPro;
using UiScroll.Code.Services.Factories.UIFactory;
using UnityEngine;
using UnityEngine.UI;

namespace UiScroll.Code.ScrollUi
{
    public class TitleView : MonoBehaviour
    {
        private IUIFactory _uiFactory;

        [SerializeField] private Button _find;
        [SerializeField] private Button _viewAll;
        [SerializeField] private TMP_InputField _field;

        public void Build(IUIFactory uiFactory)
        {
            _uiFactory = uiFactory;
            _viewAll.onClick.AddListener(ShowAll);
            _find.onClick.AddListener(Find);
        }

        private void ShowAll()
        {
            foreach (UiItemView uiItemView in _uiFactory.UiItemViews)
            {
                uiItemView.gameObject.SetActive(true);
            }
        }

        private void Find()
        {
            if (_field.text == String.Empty)
            {
                return;
            }
            
            foreach (UiItemView uiItemView in _uiFactory.UiItemViews)
            {
                uiItemView.gameObject.SetActive(false);
            }
            
            foreach (UiItemView itemView in _uiFactory.UiItemViews)
            {
                Regex regex = new Regex(_field.text.ToLower());
                MatchCollection matches = regex.Matches(itemView._name.text.ToLower());
                
                foreach (Match match in matches)
                {
                    itemView.gameObject.SetActive(true);
                }
                
                string input = itemView._phoneNumber.text.ToLower().Trim();
                string output = Regex.Replace(input, @"\s+", "");
                MatchCollection matches2 = regex.Matches(output);
                
                foreach (Match match in matches2)
                {
                    itemView.gameObject.SetActive(true);
                }
            }
            
            _field.text = String.Empty;
        }
    }
}