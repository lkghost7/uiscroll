using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UiScroll.Code.ScrollUi
{
    public class UiItemView : MonoBehaviour
    {
        [SerializeField] private GameObject _itemParent;
        [SerializeField] private RectTransform _rectTransform;
        [SerializeField] private Text _title;
        [SerializeField] public Text _name;
        [SerializeField] public Text _phoneNumber;
        [SerializeField] private Image _image;

        // public string Name { get; set; }
        // public string PhoneNumber { get; set; }
         
        private int Id;
        private int _resolutionHeight;

        public void InitData(int id, string nickName, string phone, string title, bool isTitle = false)
        {
            Id = id;
            _title.text = title;
            _name.text = nickName;
            _phoneNumber.text = phone;

            // Name = nickName;
            // PhoneNumber = phone;

            if (isTitle)
            {
                // _image.color = Color.gray;
                _title.gameObject.SetActive(true);
            }
        }

        private void Awake() => CheckResolutionScreen();

        private void Update()
        {
            if (_rectTransform.position.y >= _resolutionHeight || _rectTransform.position.y <= 0)
            {
                DisableItem();
            }
            else
            {
                EnableItem();
            }
        }

        private void EnableItem()
        {
            if (!_itemParent.activeSelf)
                _itemParent.SetActive(true);
        }

        private void DisableItem()
        {
            if (_itemParent.activeSelf)
                _itemParent.SetActive(false);
        }

        private void CheckResolutionScreen() => _resolutionHeight = Screen.height;
    }
}