using UnityEngine;

namespace UiScroll.Code.ScrollUi
{
    public class ScrollView : MonoBehaviour
    {
        [SerializeField] private Transform _content;

        public Transform GetParentContent() => _content;
    } 
}
