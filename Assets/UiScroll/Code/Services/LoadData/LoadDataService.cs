using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UiScroll.Code.Data;
using UnityEngine;
using UnityEngine.Networking;

namespace UiScroll.Code.Services.LoadData
{
    public class LoadDataService : MonoBehaviour
    {
        public event Action<int> OnCountLoad;
        public event Action OnLoadComplete;

        private GameConfig _gameConfig;
        private int Count;

        public void LoadData(GameConfig gameConfig, Action<UnityWebRequest> callBack = null)
        {
            _gameConfig = gameConfig;
            // StartCoroutine(LoadUserData(callBack));
            StartCoroutine(LoadUserText(_gameConfig.LinkDataText));
        }

        private IEnumerator LoadUserData(Action<UnityWebRequest> callback = null)
        {
            _gameConfig.UiItems.Clear();
            UnityWebRequest request = UnityWebRequest.Get(_gameConfig.LinkData);
            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                Debug.Log($"{request.error}: {request.downloadHandler.text}");
            }
            else
            {
                List<Dictionary<string, object>> data =
                    JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(request.downloadHandler.text);

                for (var i = 0; i < data.Count; i++)
                {
                    UiItem uiItem = new UiItem();
                    uiItem.ID = i;

                    var val = data[i];
                    foreach (KeyValuePair<string, object> keyValuePair in val)
                    {
                        if (keyValuePair.Key.Contains(_gameConfig.Username))
                        {
                            uiItem.NickName = keyValuePair.Value.ToString();
                        }

                        if (keyValuePair.Key.Contains(_gameConfig.Points))
                        {
                            try
                            {
                                // uiItem.Score = Convert.ToInt32(keyValuePair.Value);
                            }
                            catch (Exception e)
                            {
                                Debug.Log($"Chek data file, error {e}");
                                throw;
                            }
                        }

                        if (keyValuePair.Key.Contains(_gameConfig.AvatarUrl))
                        {
                            StartCoroutine(LoadUserPhoto(keyValuePair.Value.ToString(), i, uiItem, LoadPhotoComplete));
                        }
                    }

                    _gameConfig.AddUiItem(uiItem);
                }
            }

            callback?.Invoke(request);
        }

        private IEnumerator LoadUserPhoto(String urlPhoto, int ID, UiItem uiItem, Action<int> callback = null)
        {
            UnityWebRequest unityWebRequest = UnityWebRequestTexture.GetTexture(urlPhoto);
            yield return unityWebRequest.SendWebRequest();

            if (unityWebRequest.isNetworkError || unityWebRequest.isHttpError)
            {
                Debug.Log($"{unityWebRequest.error}: {unityWebRequest.downloadHandler.text}");
            }
            else
            {
                Texture texture = ((DownloadHandlerTexture) unityWebRequest.downloadHandler).texture;
                Sprite sprite = Sprite.Create((Texture2D) texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);

                _gameConfig.AddPhoto(uiItem, sprite);
            }

            callback?.Invoke(ID);
        }

        private IEnumerator LoadUserText(string url, Action<string> callback = null)
        {
            UnityWebRequest unityWebRequest = UnityWebRequest.Get(url);

            yield return unityWebRequest.SendWebRequest();

            string fileText = String.Empty;

            if (unityWebRequest.isNetworkError || unityWebRequest.isHttpError)
            {
                Debug.Log($"{unityWebRequest.error}: {unityWebRequest.downloadHandler.text}");
            }
            else
            {
                // print(unityWebRequest.downloadHandler.text + " unityWebRequest");

                fileText = unityWebRequest.downloadHandler.text;

                StringReader reader = new StringReader(fileText);
                string line;
                int count = 0;
                while ((line = reader.ReadLine()) != null)
                {
                    UiItem uiItem = new UiItem();
                    uiItem.ID = count;
                    char firstChar = line[0];

                    if (firstChar == '-')
                    {
                        uiItem.isTitle = true;
                        uiItem.Title = line;
                        // print("find -");
                    }
                    else
                    {
                        string[] entities = line.Split('+');
                        uiItem.NickName = entities[0];
                        uiItem.PhoneNumber = entities[1];
                        // Debug.Log(entities[0] + " 1");
                        // Debug.Log(entities[1] + " 2");
                    }

                    count++;
                    _gameConfig.AddUiItem(uiItem);
                }
            }

            OnLoadComplete?.Invoke();
        }

        private void LoadPhotoComplete(int ID)
        {
            Count++;
            OnCountLoad?.Invoke(Count);

            if (_gameConfig.UiItems.Count == Count)
                OnLoadComplete?.Invoke();
        }
    }
}