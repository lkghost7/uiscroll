using UiScroll.Code.Data;
using UnityEngine;

namespace UiScroll.Code.Services.Providers.StaticDataProvider
{
    public class StaticDataProvider : IStaticDataProvider
    {
        private const string GameConfigPath = "StaticData/Game Config";
        
        public GameConfig GetGameConfig() => Resources.Load<GameConfig>(GameConfigPath);

    }
}