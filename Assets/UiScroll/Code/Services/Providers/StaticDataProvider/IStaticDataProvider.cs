using UiScroll.Code.Data;
using UiScroll.Code.Infrastructure.ServiceContainer;

namespace UiScroll.Code.Services.Providers.StaticDataProvider
{
    public interface IStaticDataProvider : IService
    {
        GameConfig GetGameConfig();
    }
}