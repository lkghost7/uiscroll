using UiScroll.Code.Infrastructure.ServiceContainer;
using UnityEngine;

namespace UiScroll.Code.Services.Providers.AssetProvider
{
    public interface IAssetProvider : IService
    {
        GameObject GetUiRootPrefab();
        GameObject GetScrollViewPrefab();
        GameObject GetTitleView();
        GameObject GetUiItemViewPrefab();
    }
}