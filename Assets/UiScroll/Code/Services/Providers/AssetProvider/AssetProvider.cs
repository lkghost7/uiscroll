using UnityEngine;

namespace UiScroll.Code.Services.Providers.AssetProvider
{
    public class AssetProvider : IAssetProvider
    {
        private const string UIRootPrefab = "Prefabs/UiRoot";
        private const string ScrollPrefabPath = "Prefabs/ScrollView";
        private const string TitlePrefabPath = "Prefabs/TitleView";
        private const string UiItemPrefabPath = "Prefabs/UiItem";

        public GameObject GetUiRootPrefab() => Resources.Load<GameObject>(UIRootPrefab);
        public GameObject GetScrollViewPrefab() => Resources.Load<GameObject>(ScrollPrefabPath);
        public GameObject GetTitleView() => Resources.Load<GameObject>(TitlePrefabPath);
        public GameObject GetUiItemViewPrefab() => Resources.Load<GameObject>(UiItemPrefabPath);
    }
}