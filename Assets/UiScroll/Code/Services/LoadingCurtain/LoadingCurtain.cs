using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace UiScroll.Code.Services.LoadingCurtain
{
    public class LoadingCurtain : MonoBehaviour, ILoadingCurtain
    {
        public CanvasGroup _curtain;
        public Text loadingText;

        public void UpdateLoadingText(int text) => loadingText.text = text.ToString();

        public void Show(Action showIsDone = null)
        {
            gameObject.SetActive(true);
            _curtain.DOFade(1, 1f).OnComplete(() => { showIsDone?.Invoke(); });
        }
      
        public void Hide() => _curtain.DOFade(0, 1f).OnComplete(() => { gameObject.SetActive(false); });

        private void Awake()
        {
            _curtain.alpha = 1f;
            DontDestroyOnLoad(this);
        }
    }
}