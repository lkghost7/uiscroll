using System;
using UiScroll.Code.Infrastructure.ServiceContainer;

namespace UiScroll.Code.Services.LoadingCurtain
{
    public interface ILoadingCurtain : IService
    {
        void Show(Action onComplete);
        void Hide();
    }
}