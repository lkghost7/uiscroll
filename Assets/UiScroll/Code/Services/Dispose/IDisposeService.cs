using System;
using UiScroll.Code.Infrastructure.ServiceContainer;

namespace UiScroll.Code.Services.Dispose
{
    public interface IDisposeService : IService
    {
        void RegisterDisposable(IDisposable disposable, string scene);
        void DisposeAll(string scene);
    }
}