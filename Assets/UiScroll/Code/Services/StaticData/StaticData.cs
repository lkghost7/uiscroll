using UiScroll.Code.Data;
using UiScroll.Code.Services.Providers.StaticDataProvider;

namespace UiScroll.Code.Services.StaticData
{
    public class StaticData : IStaticData
    {
        public GameConfig GameConfig { get; private set; }
        
        private readonly IStaticDataProvider _staticDataProvider;

        public StaticData(IStaticDataProvider staticDataProvider) =>
            _staticDataProvider = staticDataProvider;

        public void LoadStaticData()
        {
            GameConfig = _staticDataProvider.GetGameConfig();
        }
    }
}