using UiScroll.Code.Data;
using UiScroll.Code.Infrastructure.ServiceContainer;

namespace UiScroll.Code.Services.StaticData
{
    public interface IStaticData : IService
    {
        GameConfig GameConfig { get; }

        void LoadStaticData();
    }
}