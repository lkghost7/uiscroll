using System;
using UiScroll.Code.Infrastructure.ServiceContainer;

namespace UiScroll.Code.Services.SceneLoader
{
    public interface ISceneLoader : IService
    {
        void LoadScene(string sceneName, Action onLoaded = null);
    }
}