using System.Collections.Generic;
using UiScroll.Code.Data;
using UiScroll.Code.Infrastructure.StateMachine;
using UiScroll.Code.ScrollUi;
using UiScroll.Code.Services.Dispose;
using UiScroll.Code.Services.Providers.AssetProvider;
using UiScroll.Code.Services.StaticData;
using UnityEngine;

namespace UiScroll.Code.Services.Factories.UIFactory
{
    public class UIFactory : IUIFactory
    {
        public ScrollView ScrollView { get; private set; }
        public TitleView TitleView { get; private set;}
        public List<UiItemView> UiItemViews { get; private set; } = new List<UiItemView>();
        public string ActiveScene { get; set; }

        private readonly IAssetProvider _assetProvider;
        private readonly IGameStateMachine _stateMachine;
        private readonly IStaticData _staticData;
        private readonly IDisposeService _disposeService;

        public UIFactory(IGameStateMachine stateMachine, IAssetProvider assetProvider, IStaticData staticData,
            IDisposeService disposeService)
        {
            _disposeService = disposeService;
            _staticData = staticData;
            _stateMachine = stateMachine;
            _assetProvider = assetProvider;
        }

        public GameObject CreateRootCanvas() => Object.Instantiate(_assetProvider.GetUiRootPrefab());

        public ScrollView CreateScrollView(Transform uIRoot)
        {
            return ScrollView = Object.Instantiate(_assetProvider.GetScrollViewPrefab(), uIRoot).GetComponent<ScrollView>();
        }

        public TitleView CreateTitleView(Transform uIRoot)
        {
            TitleView = Object.Instantiate(_assetProvider.GetTitleView(), uIRoot).GetComponent<TitleView>();
            TitleView.Build(this);
            return TitleView;
        }

        public void CreateUiItems()
        {
            for (int i = 0; i < _staticData.GameConfig.UiItems.Count; i++)
            {
                UiItem item = _staticData.GameConfig.UiItems[i];

                if (item.isTitle)
                {
                    UiItemView uiItemView = Object.Instantiate(_assetProvider.GetUiItemViewPrefab(), ScrollView.GetParentContent()).GetComponent<UiItemView>();
                    uiItemView.InitData(item.ID, item.NickName, item.PhoneNumber, item.Title, true);
                    UiItemViews.Add(uiItemView);
                }
                else
                {
                    UiItemView uiItemView = Object.Instantiate(_assetProvider.GetUiItemViewPrefab(), ScrollView.GetParentContent()).GetComponent<UiItemView>();
                    uiItemView.InitData(item.ID, item.NickName, item.PhoneNumber, item.Title);
                    UiItemViews.Add(uiItemView);
                }
            }
        }
    }
}