using System.Collections.Generic;
using UiScroll.Code.Infrastructure.ServiceContainer;
using UiScroll.Code.ScrollUi;
using UnityEngine;

namespace UiScroll.Code.Services.Factories.UIFactory
{
    public interface IUIFactory : IService
    {
        public ScrollView ScrollView { get; }
        public TitleView TitleView { get; }
         
        GameObject CreateRootCanvas();
        ScrollView CreateScrollView(Transform uIRoot);
        TitleView CreateTitleView(Transform uIRoot);
        List<UiItemView> UiItemViews { get;}
        void CreateUiItems();
        string ActiveScene { get; set; }
    }
}